# Spotify-bg

Uses the Spotify API to get your most litsened songs and paste their covers on a wallpaper.

This is a work in progress.

### TODO

- [ ] Scale images to fill whole wallpaper
- [ ] Do not repeat same icons
- [ ] Add padding option
- [x] Allow artists instead of only songs
- [ ] Allow filtering of albums, artists or songs
- [x] Make some arguments optional
- [x] Config file instead of arguments
- [ ] Rewrite in compiled language (Go, Rust...)

![](background.png)