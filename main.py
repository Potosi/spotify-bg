#!/usr/bin/python3

import os
import os.path
import sys
import json
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import spotipy.util as util
import PIL
from PIL import Image
import requests
import configparser
import math


if not len(sys.argv) == 3:
    print("usage: spotify-bg [input-image] [output-image]")
    quit()

config = configparser.ConfigParser()
config.read("config.ini")

if not "GLOBAL" in config:
    print("Config file doesn't have the 'GLOBAL' field.")
    quit()

conf = config["GLOBAL"]
savepath = str(conf["savepath"])

if not os.path.isdir(savepath):
    print("Please specify a proper 'savepath' in your config file.")
    quit()

#center = conf.get("center", "top_right") # MUST ADD. ALSO ADD COLUMNS AND ROWS OPTIONS.
background_name = str(sys.argv[1])
output = str(sys.argv[2])
background = Image.open(background_name)
ammount = int(conf.get("ammount", 25))
spacing = int(conf.get("spacing", 0))
#padding = int(conf.get("padding", spacing)) # MUST ADD.
choice = str(conf.get("choice", "current_user_top_tracks")) # Maybe come up with a better name?
username = str(conf.get("username", ""))
client_id = str(conf.get("client_id", ""))
client_secret = str(conf.get("client_secret", ""))
redirect_uri = str(conf.get("redirect_uri", ""))
time_range = str(conf.get("time_range", "medium_term"))
scope = "user-top-read"
token = util.prompt_for_user_token(
    username, scope, client_id, client_secret, redirect_uri
)
sp = spotipy.Spotify(auth=token)
xspacing = spacing
yspacing = spacing
x = spacing # There must be a way to avoid this.
y = spacing

if spacing == 0:
    imgsize = int(
        conf.get(
            "imgsize",
            math.floor(math.sqrt(background.size[0] / math.sqrt(ammount)))
            * math.floor(math.sqrt(background.size[1] / math.sqrt(ammount))),
        )
    )
else:
    imgsize = int(conf.get("imgsize", 100))  # Need to work out this. Same as above but with spacing.
    # Because of this, image scaling only works if spacing == 0.
    # Maybe 'do not scale image above original value'? - To avoid bad image quality

file = "top" + str(ammount) + time_range + choice + ".json"

print("spacing:   ", spacing)
print("img_size:  ", imgsize)
print("ammount:   ", ammount)
print("save_path: ", savepath)
print("json:      ", file)
print("choice:    ", choice)
print("Using background image:", background_name)
print("Background size:", background.size)

if not os.path.isfile(file):

    if conf.get("choice") == "current_user_top_tracks":
        results = sp.current_user_top_tracks(limit=ammount, time_range=time_range)
    if conf.get("choice") == "current_user_top_artists":
        results = sp.current_user_top_artists(limit=ammount, time_range=time_range)

    for song in range(ammount):
        list = []
        list.append(results)
    with open(file, "w", encoding="utf-8") as f:
        json.dump(list, f, ensure_ascii=False, indent=4)

with open(file) as f:
    data = json.load(f)

list_of_results = data[0]["items"]
times = 0

for result in list_of_results:

    if conf.get("choice") == "current_user_top_artists":
        result["name"]
        this_artists_name = result["name"]
        url = result["images"][0]["url"]
        imagename = os.path.join(savepath, str(this_artists_name) + ".png")

    else:
        result["album"]
        list_of_songs = result["name"]
        url = result["album"]["images"][0]["url"]
        imagename = os.path.join(savepath, list_of_songs + ".png")

    if not os.path.isfile(imagename):
        print("Downloading ", imagename + ":", url)
        response = requests.get(url)
        open(imagename, "wb").write(response.content)

    image = Image.open(imagename)
    image = image.resize((imgsize, imgsize))

    times += 1

    # In case the file has more entries than the ammount specified.
    # That should have been fixed anyway with appending info to the json filename
    if times >= ammount:
        background.save(output, "PNG")
        quit()

    if times == 1:
        background.paste(image, (xspacing, yspacing))
        xspacing = x * 2
    if times != 1:
        background.paste(image, (imgsize + xspacing, yspacing))
        xspacing = xspacing + x + imgsize
        if background.size[0] < xspacing + x + imgsize * 2:
            xspacing = x
            yspacing = yspacing + y + imgsize
            times = 0
        if background.size[1] < yspacing + x + imgsize:
            background.save(output, "PNG")
            quit()


background.save(output, "PNG")
